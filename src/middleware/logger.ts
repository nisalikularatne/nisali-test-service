import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common'
import { catchError, tap } from 'rxjs/operators'
import { throwError } from 'rxjs'
@Injectable()
export class InsuranceServiceLogger implements NestInterceptor {
  private logger = new Logger('HTTP');
  intercept(context: ExecutionContext, next: CallHandler) {
    const req = context.switchToHttp().getRequest();
    const { statusCode } = context.switchToHttp().getResponse();
    const { originalUrl, method, params, query, body,headers,user } = req;

    return next.handle().pipe(
      catchError(err => {
        this.logger.error(JSON.stringify({
          originalUrl,
          method,
          params,
          query,
          headers,
          user,
          body,
          err,
        }))
        return throwError(err);
      }),
      tap((data) =>
        this.logger.log(JSON.stringify({
          originalUrl,
          method,
          params,
          query,
          body,
          statusCode,
          headers,
          user,
          data,
        }))
      )
    );
  }
}