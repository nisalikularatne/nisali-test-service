import { Controller, Get } from '@nestjs/common';
import { AppService } from 'src/app.service';
import { ApiExcludeEndpoint } from '@nestjs/swagger';

@Controller('health')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiExcludeEndpoint()
  getHealth(): string {
    return this.appService.getStatus();
  }
}
