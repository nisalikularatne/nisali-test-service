import { Observable } from 'rxjs'
import { Metadata } from 'grpc';

// Define interface:
export interface PaymentInterface {
  createPaymentIntent(data: CreatePaymentIntent, metadata: Metadata): Observable<any>;
  createPaymentIntentOneTime(data:CreatePaymentIntentOneTime, meta:Metadata): Observable<any>;
  refundPaymentIntent(data: RefundPaymentIntent, metadata: Metadata): Observable<any>;
  getPaymentIntents(data: GetPaymentIntents, metadata: Metadata): Observable<any>;
  syncPaymentMethod(data: SyncPaymentMethod, metadata: Metadata): Observable<any>;
}

export interface CreatePaymentIntent {
  userUUID: string
  paymentMethodId?: string
  amount: number
  description: string
  entity: string
  entityId: string
  capture: boolean
  ewalletsData: any
}

export interface CreatePaymentIntentOneTime {
  userUUID: string
  amount: number
  description: string
  entity: string
  entityId: string
  capture: boolean
}

export interface RefundPaymentIntent {
  paymentIntentId: string;
  amount: number;
  description?: string;
}


export interface GetPaymentIntents {
  paymentIntentIds: string[];
}

export interface SyncPaymentMethod {
  paymentIntentId: string;
  userUUID: string;
  email: string;
  userId: number;
}
