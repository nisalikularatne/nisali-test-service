export const roundedNetAmount = (netAmount:number):number=>{
  const roundedNetAmount = Math.round(netAmount*20)/20
  return Number(roundedNetAmount.toFixed(2))
}

export const mapGender = (gender:string):string=>{
  if(gender==='male'){
    return 'M'
  }
  else if(gender==='female'){
    return 'F'
  }
}

export const mapPhoneNumbers=(mobilePrefix:string,mobile:string)=>{
  const mobilePrefixMap = mobilePrefix.slice(1)+mobile.slice(0,2);
  const mobileMap = mobile.slice(2)
  return {mobilePrefix:mobilePrefixMap,mobile:mobileMap}
}
