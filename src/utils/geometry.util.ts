/**
 * Geometry utility functions
 *
 */
export class Geometry {
  static distance(origins: [number, number], destinations: [number, number]): any {
    const [lat1, lng1] = origins;
    const [lat2, lng2] = destinations;

    if (!lat1 || !lat1 || !lat2 || !lng2) {
      return {
        text: null,
        value: null
      }
    }

    const unit = 6371e3; // metres
    const φ1 = (lat1 * Math.PI) / 180; // φ, λ in radians
    const φ2 = (lat2 * Math.PI) / 180;
    const Δφ = ((lat2 - lat1) * Math.PI) / 180;
    const Δλ = ((lng2 - lng1) * Math.PI) / 180;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const result = { distance : unit * c, unit: 'km', smallUnit: 'm', factor: 1000, smallBorder: 0.9  }

    // Format result:
    if (result.distance < result.smallBorder) {
      let distance = result.distance * result.factor

      distance = Math.round(distance / 50) * 50
      return {
        text: `${distance} ${result.smallUnit}`,
        value: Math.round(result.distance),
      }
    }

    // Return the result:
    return {
      text: `${Math.round(result.distance / 1000)} ${result.unit}`,
      value: Math.round(result.distance),
    }
  }
}
