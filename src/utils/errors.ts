export const allianzUbbErrors = [
  {
    code: 'CM001',
    description: 'Driver Age'
  },
  {
    code: 'CM002',
    description: 'HRTV'
  },
  {
    code: 'CM003',
    description: 'Vehicle Capacity'
  },
  {
    code: 'CM004',
    description: 'Vehicle Type Referred'
  },
  {
    code: 'CM006',
    description: 'Vehicle age'
  },
  {
    code: 'CM007',
    description: 'Sum Insured Exceed'
  },
  {
    code: 'CM008',
    description: 'Excess Waiver'
  },
  {
    code: 'CM0010',
    description: 'Upgrade To Comprehensive'
  },
  {
    code: 'CM0011',
    description: 'High Performance'
  },
  {
    code: 'CM0019',
    description: 'TP Coverage Upgrade'
  },
  {
    code: 'UBBE003',
    description: 'Referred Claims Experience'
  },
  {
    code: 'UBBE004',
    description: 'If the user has 2/3 or more claim experience'
  },
  {
    code: 'UBBE005',
    description: 'Referred recon car'
  },
  {
    code: 'UBBE006',
    description: 'Recon Risk - The checking would be if the customer chooses “no“ then it will be referred'
  }


]
