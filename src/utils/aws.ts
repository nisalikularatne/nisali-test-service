// MODULE DEPENDENCIES
import * as moment from 'moment';
import * as tk from 'timekeeper';
import * as AWS from 'aws-sdk';
import * as dotenv from 'dotenv';
import _ = require('lodash');
import { format } from 'date-fns'
import { ServiceUnavailableException } from '@nestjs/common';
import { Logger } from '@nestjs/common';
dotenv.config();

AWS.config.update({ //  global-upload (IAM user)
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
});
export const S3 = {
  getSignedUrl(bucket: string, key: string, cacheFriendly?: boolean): Promise<any> {
    try {
      const s3 = new AWS.S3()
      let url;
      if (cacheFriendly) {
        url = tk.withFreeze(moment().subtract(1, 'month').endOf('month').toDate(), () => {
          const predatedUrl = s3.getSignedUrl('getObject', {
            Bucket: bucket,
            Key: key,
            Expires: 31 * 24 * 60 * 60,
            ResponseContentDisposition: 'inline',
          });

          return predatedUrl;
        });
        // if (process.env.env === 'production' && process.env.AWS_CLOUD_FRONT_DOMAIN) {
        //   const regex = RegExp(`${bucket}.s3.${process.env.AWS_REGION}.amazonaws.com`);
        //   url = url.replace(regex, process.env.AWS_CLOUD_FRONT_DOMAIN);
        // }
      } else {
        url = s3.getSignedUrl('getObject', {
          Bucket: bucket,
          Key: key,
          Expires: 30 * 24 * 60 * 60,
          ResponseContentDisposition: 'inline',
        });
      }
      return url;
    } catch (e) {
      console.log(e);
      return;
    }
  },
  async upload(file: { filename: string; file: Buffer|string }, bucket: string, path: string, subPath?: string, options = { isPublic: false }): Promise<any> {
    const { filename } = file;
    const { isPublic } = options;
    const name = _.lowerCase(_.head(filename.split('.')));
    const ext = _.lowerCase(_.last(filename.split('.')));
    const now = new Date();
    let contentType;
    let key = `${path}/${format(now, 'yyyy/MM/dd/hhmmss')}-${name}.${ext}`;

    if (_.size(subPath) > 0) {
      key = `${path}/${subPath}/${format(now, 'yyyy/MM/dd/hhmmss')}-${name}.${ext}`;
    }
    if(ext==='pdf'){
      contentType = `application/pdf`;
    }
    else{
      contentType = `image/${ext === 'jpg' ? 'jpeg' : ext}`;
    }

    const acl = (isPublic) ? 'public-read' : 'private';

    const params = { params: { Bucket: bucket, Key: key, ContentType: contentType, ACL: acl, Body: file.file } };

    const s3 = new AWS.S3(params);

    try {
      await s3.upload(params.params).promise()
      const url = this.getSignedUrl(bucket, key);
  
      return { bucket, key, filename: `${name}.${ext}`, contentType, url };
    } catch (error) {
      Logger.error('upload error',error);
      throw new ServiceUnavailableException('Upload fail to S3');
    }
  },
};
