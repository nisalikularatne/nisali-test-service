import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

import helmet from 'fastify-helmet';
import { AppModule } from 'src/app.module';
import fastifyMultipart from 'fastify-multipart';

async function bootstrap() {
  const fastifyAdapter = new FastifyAdapter({
    logger: true,
    bodyLimit: 2904857
  })

  fastifyAdapter.register(fastifyMultipart)

  const app = await NestFactory.create<NestFastifyApplication>(AppModule,  fastifyAdapter);
  
  // Security:
  app.register(helmet, {
    contentSecurityPolicy: {
      directives: {
        defaultSrc: [`'self'`],
        styleSrc: [`'self'`, `'unsafe-inline'`],
        imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
        scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
      },
    },
  })

  app.enableCors();

  // Set validation pipe:
  app.useGlobalPipes(new ValidationPipe({ transform: true, errorHttpStatusCode: 422, whitelist: true }));

  const options = new DocumentBuilder()
    .setTitle('Insurance Service - API')
    .setDescription('The insurance service API is our proprietary REST API implementation.')
    .setVersion('1.0')
    .addServer('https://api-alpha.socar.my/insurances')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options, {
    ignoreGlobalPrefix: true,
  });
  
  SwaggerModule.setup('docs', app, document);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      url: `0.0.0.0:${process.env.RPC_PORT || 5000}`,
      package: 'insurance',
      protoPath: 'src/proto/insurance.proto',
    },
  });

  // Run all servers:
  await app.startAllMicroservicesAsync();
  await app.listen(Number(process.env.APP_PORT) || 4000, '0.0.0.0');
}

bootstrap();
