import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { AppController } from 'src/app.controller';
import { AppService } from 'src/app.service';
import { ApiModule } from 'src/modules/api/api.module';
import { AuthModule } from 'src/auth/auth.module';
import { SequelizeModule } from 'src/config/sequelize.config';
import { BullModule } from '@nestjs/bull';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { InsuranceServiceLogger } from 'src/middleware/logger'


@Module({
  imports: [
    SequelizeModule,
    ApiModule,
    AuthModule,
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST || 'localhost',
        port: Number(process.env.REDIS_PORT || 6379),
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService,{
    provide: APP_INTERCEPTOR,
    useClass: InsuranceServiceLogger,
  }],
})

export class AppModule {}
