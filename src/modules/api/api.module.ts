import { Module, HttpModule, CacheModule } from '@nestjs/common'
import { SequelizeModule } from '@nestjs/sequelize'
import { BullModule } from '@nestjs/bull'
// RPC config:
import ClientsModule from 'src/config/services.config'


@Module({
  imports: [
    SequelizeModule.forFeature([
    ]),
    HttpModule,
    CacheModule.register(),
    ClientsModule,
  ],
  controllers: [
  ],
  providers: [

  ],
  exports:[

  ]
})

export class ApiModule {
}
