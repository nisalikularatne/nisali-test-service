import { Module } from '@nestjs/common';
import { SequelizeModule as sequelize } from '@nestjs/sequelize';
import { parse } from 'pg-connection-string'

const db = parse(process.env.DB_URI)

@Module({
  imports: [
    sequelize.forRoot({
      dialect: 'postgres',
      username: db.user,
      password: db.password,
      host: db.host,
      database: db.database,
      logging: false,
      define: {
        underscored: true,
      },
      autoLoadModels: true,
      synchronize: false,
    }),
  ],
})

export class SequelizeModule {}