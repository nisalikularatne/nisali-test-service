import { Transport, ClientsModule } from '@nestjs/microservices';
import * as dotenv from 'dotenv';
import * as grpc from 'grpc';
// To load env files on start:
dotenv.config();

export default ClientsModule.register([
  {
    name: 'ZONE_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.ZONE_SERVICE_HOST,
      package: 'zone',
      protoPath: 'src/proto/zone.proto',
    },
  },
  {
    name: 'PRICING_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.PRICING_SERVICE_HOST,
      package: 'pricing',
      protoPath: 'src/proto/pricing.proto',
    },
  },
  {
    name: 'BOOKING_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.BOOKING_SERVICE_HOST,
      package: 'booking',
      protoPath: 'src/proto/booking.proto',
    },
  },
  {
    name: 'PROMO_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.USERPROMO_SERVICE_HOST,
      package: 'promo',
      protoPath: 'src/proto/promo.proto'
    }
  },
  {
    name: 'USER_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.USER_SERVICE_HOST,
      package: 'user',
      protoPath: 'src/proto/user.proto',
    },
  },
  {
    name: 'PROMO_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.USERPROMO_SERVICE_HOST,
      package: 'promo',
      protoPath: 'src/proto/promo.proto'
    }
  },
  {
    name: 'TREVO_USER_PACKAGE',
    transport: Transport.GRPC,
    options: {
      credentials: grpc.credentials.createSsl(),
      url: process.env.COMMON_USER_SERVICE_HOST,
      package: 'user',
      protoPath: 'src/proto/trevo_users.proto',
    },
  },
  {
    name: 'PAYMENT_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.PAYMENT_SERVICE_HOST,
      package: 'payment',
      protoPath: 'src/proto/payment.proto',
    },
  },
  {
    name: 'MESSAGING_PACKAGE',
    transport: Transport.GRPC,
    options: {
      url: process.env.MESSAGING_SERVICE_HOST,
      package: 'messaging',
      protoPath: 'src/proto/messaging.proto',
    },
  },
])
