import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';

// To load env files on start:
dotenv.config();

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: Buffer.from(process.env.AUTH_PUBLIC_KEY, 'hex').toString('utf8').replace(/\\n/gm, '\n'),
    });
  }

  
  async validate(payload: Record<string, any>): Promise<any> {
    return payload.user;  // Return user object:
  }
}