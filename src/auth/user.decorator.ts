import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const User = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;

    return data ? user?.[data] : user;
  },
)

export class UserDto {
  id: number
  email: string
  firstName: string
  lastName: string
  displayName: string
  gender: string
  dob: string
  phoneCountryCode: string
  phoneNumber: string
  status: string
  picture: string
  createdAt: Date
  updatedAt: Date
  uuid: string
  deletedAt: Date
  nationality: string
  meta: Record<string, any>
}