import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  // Return status:
  getStatus(): any {
    return {
      status: 'ok'
    }
  }
}
