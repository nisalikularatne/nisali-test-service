#!/bin/bash

path="prod"

if [[ $3 == "develop" ]]; then
path="alpha"
fi

cat <<EOF >deploy/overlays/$path/latest-image.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $1
  labels:
    app: $1
spec:
  template:
    spec:
      containers:
      - name: $1
        image: $2
EOF